import "./App.css";
import VAMK from "./assets/VAMK_logo_väri_posit_cmyk_L.png";

function App() {
  return (
    <div className="App">
      <img className="logo" src={VAMK} alt="VAMK logo" />
      <span className="Opiskelijanimi">Opiskelijan nimi</span>
      <span className="Opiskelijanumero">Opiskelijanumero</span>
      <span className="Ala">Tietotekniikka</span>
      <span className="Sahkoposti">malli@edu.vamk.fi</span>
      <span className="Osoite">Wolffintie 30, FI-65200 VAASA, Finland</span>
    </div>
  );
}

export default App;
